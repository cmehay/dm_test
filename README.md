# Playlist API

## Run

#### Using docker and docker-compose
```
$ make run
```

To stop everything

```
$ make down
```

#### Run

python 3 is required

```
$ python3 -m Playlist
```

#### Install

python 3 is required

```
$ pip install .
$ api_run_server
```

## Tests

```
$ make test
```

Stop mysql container after

```
$ make down
```

## Setup

Setup using environment variable (values are default)

- `API_BIND`: `127.0.0.1`
- `API_PORT`: `8000`
- `DEBUG`: `False` (enable Bottle debug mode)
- `MYSQL_HOST`: `localhost`
- `MYSQL_USER`: `root`
- `MYSQL_PASSWORD`: *empty*
- `MYSQL_DATABASE`: `db`


## API
| Method  | Route     |
|---------|-----------|
|`GET`    | `/videos` |
|`GET`    | `/videos/<id>` |
|`GET`    |`/playlists`|
|`POST`   |`/playlists`|
|`GET`    |`/playlists/<id>`|
|`PUT`    |`/playlists/<id>`|
|`DELETE` |`/playlists/<id>`|
|`GET`    |`/playlists/<id>/videos`|
|`PUT`    |`/playlists/<id>/videos/<id>`|
|`DELETE` |`/playlists/<id>/videos/<id>`|

### Examples

List videos

```
$ http localhost:8000/videos
HTTP/1.0 200 OK
Content-Length: 7111
Content-Type: application/json
Date: Tue, 28 Aug 2018 22:53:32 GMT
Server: WSGIServer/0.2 CPython/3.7.0

{
    "data": [
        {
            "id": "1",
            "thumbnail": "path/to/video_1.png",
            "title": "video_1"
        },
        {
            "id": "2",
            "thumbnail": "path/to/video_2.png",
            "title": "video_2"
        },
.....

```

Create playlist
```
$ http POST localhost:8000/playlists name='test playlist'
HTTP/1.0 201 Created
Content-Length: 84
Content-Type: application/json
Date: Tue, 28 Aug 2018 22:55:44 GMT
Server: WSGIServer/0.2 CPython/3.7.0

{
    "id": "2aa310f3-b920-4521-8edb-b9d521517fb1",
    "length": 0,
    "name": "test playlist"
}
```

Add video in playlist
```
$ http PUT localhost:8000/playlists/2aa310f3-b920-4521-8edb-b9d521517fb1/videos/3
HTTP/1.0 200 OK
Content-Length: 84
Content-Type: application/json
Date: Tue, 28 Aug 2018 22:57:18 GMT
Server: WSGIServer/0.2 CPython/3.7.0

{
    "id": "2aa310f3-b920-4521-8edb-b9d521517fb1",
    "length": 1,
    "name": "test playlist"
}

```

List videos in playlist
```
$ http localhost:8000/playlists/2aa310f3-b920-4521-8edb-b9d521517fb1/videos   
HTTP/1.0 200 OK
Content-Length: 79
Content-Type: application/json
Date: Tue, 28 Aug 2018 23:00:12 GMT
Server: WSGIServer/0.2 CPython/3.7.0

{
    "data": [
        {
            "id": "3",
            "thumbnail": "path/to/video_3.png",
            "title": "video_3"
        }
    ]
}

```

Remove video in playlist
```
$ http DELETE localhost:8000/playlists/2aa310f3-b920-4521-8edb-b9d521517fb1/videos/3
HTTP/1.0 200 OK
Content-Length: 84
Content-Type: application/json
Date: Tue, 28 Aug 2018 22:58:56 GMT
Server: WSGIServer/0.2 CPython/3.7.0

{
    "id": "2aa310f3-b920-4521-8edb-b9d521517fb1",
    "length": 0,
    "name": "test playlist"
}
```

Rename playlist
```
http PUT localhost:8000/playlists/2aa310f3-b920-4521-8edb-b9d521517fb1 name='test 2'
HTTP/1.0 200 OK
Content-Length: 77
Content-Type: application/json
Date: Wed, 29 Aug 2018 08:52:27 GMT
Server: WSGIServer/0.2 CPython/3.7.0

{
    "id": "2aa310f3-b920-4521-8edb-b9d521517fb1",
    "length": 0,
    "name": "test 2"
}
```
