#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from setuptools import find_packages
from setuptools import setup

setup(
    name='playlist',

    version='0.0.0.1',

    author="Christophe Mehay",

    author_email="cmehay@student.42.fr",

    description="Dailymotion playlist api",

    packages=['Playlists'],

    include_package_data=True,

    install_requires=['pymysql',
                      'bottle',
                      'pytest'],

    entry_points={
        'console_scripts': [
            'api_run_server = Playlists.__main__:main',
        ],
    },
)
