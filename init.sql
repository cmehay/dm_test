CREATE TABLE video (
    id int AUTO_INCREMENT,
    title varchar(255) NOT NULL,
    thumbnail varchar(255),
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE playlist (
    id varchar(36) NOT NULL,
    name varchar(255) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE playlist_content (
    playlist_id varchar(36) NOT NULL,
    video_id int NOT NULL,
    idx int NOT NULL,
    CONSTRAINT fk_playlist FOREIGN KEY (playlist_id)
    REFERENCES playlist(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE playlist_content
  ADD CONSTRAINT fk_video FOREIGN KEY (video_id)
  REFERENCES video(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
;

INSERT INTO video (id, title, thumbnail) VALUES
(1, 'video_1', 'path/to/video_1.png'),
(2, 'video_2', 'path/to/video_2.png'),
(3, 'video_3', 'path/to/video_3.png'),
(4, 'video_4', 'path/to/video_4.png'),
(5, 'video_5', 'path/to/video_5.png'),
(6, 'video_6', 'path/to/video_6.png'),
(7, 'video_7', 'path/to/video_7.png'),
(8, 'video_8', 'path/to/video_8.png'),
(9, 'video_9', 'path/to/video_9.png'),
(10, 'video_10', 'path/to/video_10.png'),
(11, 'video_11', 'path/to/video_11.png'),
(12, 'video_12', 'path/to/video_12.png'),
(13, 'video_13', 'path/to/video_13.png'),
(14, 'video_14', 'path/to/video_14.png'),
(15, 'video_15', 'path/to/video_15.png'),
(16, 'video_16', 'path/to/video_16.png'),
(17, 'video_17', 'path/to/video_17.png'),
(18, 'video_18', 'path/to/video_18.png'),
(19, 'video_19', 'path/to/video_19.png'),
(20, 'video_20', 'path/to/video_20.png'),
(21, 'video_21', 'path/to/video_21.png'),
(22, 'video_22', 'path/to/video_22.png'),
(23, 'video_23', 'path/to/video_23.png'),
(24, 'video_24', 'path/to/video_24.png'),
(25, 'video_25', 'path/to/video_25.png'),
(26, 'video_26', 'path/to/video_26.png'),
(27, 'video_27', 'path/to/video_27.png'),
(28, 'video_28', 'path/to/video_28.png'),
(29, 'video_29', 'path/to/video_29.png'),
(30, 'video_30', 'path/to/video_30.png'),
(31, 'video_31', 'path/to/video_31.png'),
(32, 'video_32', 'path/to/video_32.png'),
(33, 'video_33', 'path/to/video_33.png'),
(34, 'video_34', 'path/to/video_34.png'),
(35, 'video_35', 'path/to/video_35.png'),
(36, 'video_36', 'path/to/video_36.png'),
(37, 'video_37', 'path/to/video_37.png'),
(38, 'video_38', 'path/to/video_38.png'),
(39, 'video_39', 'path/to/video_39.png'),
(40, 'video_40', 'path/to/video_40.png'),
(41, 'video_41', 'path/to/video_41.png'),
(42, 'video_42', 'path/to/video_42.png'),
(43, 'video_43', 'path/to/video_43.png'),
(44, 'video_44', 'path/to/video_44.png'),
(45, 'video_45', 'path/to/video_45.png'),
(46, 'video_46', 'path/to/video_46.png'),
(47, 'video_47', 'path/to/video_47.png'),
(48, 'video_48', 'path/to/video_48.png'),
(49, 'video_49', 'path/to/video_49.png'),
(50, 'video_50', 'path/to/video_50.png'),
(51, 'video_51', 'path/to/video_51.png'),
(52, 'video_52', 'path/to/video_52.png'),
(53, 'video_53', 'path/to/video_53.png'),
(54, 'video_54', 'path/to/video_54.png'),
(55, 'video_55', 'path/to/video_55.png'),
(56, 'video_56', 'path/to/video_56.png'),
(57, 'video_57', 'path/to/video_57.png'),
(58, 'video_58', 'path/to/video_58.png'),
(59, 'video_59', 'path/to/video_59.png'),
(60, 'video_60', 'path/to/video_60.png'),
(61, 'video_61', 'path/to/video_61.png'),
(62, 'video_62', 'path/to/video_62.png'),
(63, 'video_63', 'path/to/video_63.png'),
(64, 'video_64', 'path/to/video_64.png'),
(65, 'video_65', 'path/to/video_65.png'),
(66, 'video_66', 'path/to/video_66.png'),
(67, 'video_67', 'path/to/video_67.png'),
(68, 'video_68', 'path/to/video_68.png'),
(69, 'video_69', 'path/to/video_69.png'),
(70, 'video_70', 'path/to/video_70.png'),
(71, 'video_71', 'path/to/video_71.png'),
(72, 'video_72', 'path/to/video_72.png'),
(73, 'video_73', 'path/to/video_73.png'),
(74, 'video_74', 'path/to/video_74.png'),
(75, 'video_75', 'path/to/video_75.png'),
(76, 'video_76', 'path/to/video_76.png'),
(77, 'video_77', 'path/to/video_77.png'),
(78, 'video_78', 'path/to/video_78.png'),
(79, 'video_79', 'path/to/video_79.png'),
(80, 'video_80', 'path/to/video_80.png'),
(81, 'video_81', 'path/to/video_81.png'),
(82, 'video_82', 'path/to/video_82.png'),
(83, 'video_83', 'path/to/video_83.png'),
(84, 'video_84', 'path/to/video_84.png'),
(85, 'video_85', 'path/to/video_85.png'),
(86, 'video_86', 'path/to/video_86.png'),
(87, 'video_87', 'path/to/video_87.png'),
(88, 'video_88', 'path/to/video_88.png'),
(89, 'video_89', 'path/to/video_89.png'),
(90, 'video_90', 'path/to/video_90.png'),
(91, 'video_91', 'path/to/video_91.png'),
(92, 'video_92', 'path/to/video_92.png'),
(93, 'video_93', 'path/to/video_93.png'),
(94, 'video_94', 'path/to/video_94.png'),
(95, 'video_95', 'path/to/video_95.png'),
(96, 'video_96', 'path/to/video_96.png'),
(97, 'video_97', 'path/to/video_97.png'),
(98, 'video_98', 'path/to/video_98.png'),
(99, 'video_99', 'path/to/video_99.png');
