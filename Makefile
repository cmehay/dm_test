run: mysql build
	docker-compose run --rm --service-ports playlist_api

run-debug: mysql build
	docker-compose run --rm --service-ports -e DEBUG=true playlist_api

down:
	docker-compose down

test: mysql build
	sleep 5
	docker-compose run --rm playlist_api pytest -v Playlists/tests.py

build:
	docker-compose build playlist_api

mysql:
	docker-compose up -d mysql
