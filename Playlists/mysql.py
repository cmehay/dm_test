import pymysql
import os


class Mysql(object):

    host = os.environ.get('MYSQL_HOST', 'localhost')
    user = os.environ.get('MYSQL_USER', 'root')
    password = os.environ.get('MYSQL_PASSWORD')
    db = os.environ.get('MYSQL_DATABASE', 'db')
    charset = 'utf8mb4'

    def __init__(self):
        'Open connection to database'
        self.con = pymysql.connect(host=self.host,
                                   user=self.user,
                                   password=self.password,
                                   db=self.db,
                                   charset=self.charset,
                                   cursorclass=pymysql.cursors.DictCursor)

    def __enter__(self):
        return self.con

    def __exit__(self, *args, **kwargs):
        self.con.close()
