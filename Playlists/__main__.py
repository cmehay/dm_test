import os

from bottle import run
from distutils.util import strtobool


def main():
    run(host=os.environ.get('API_BIND', '127.0.0.1'),
        port=int(os.environ.get('API_PORT', '8000')),
        debug=strtobool(os.environ.get('DEBUG', 'false')))


if __name__ == "__main__":
    main()
