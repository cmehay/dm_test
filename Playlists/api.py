from bottle import get, post, put, delete, request, abort, response

from .playlist import Video, Playlist

import uuid


@get('/videos')
def get_all_videos():
    return {'data': [video.serialize() for video in Video.get_all()]}


@get('/videos/<vid:int>')
def get_video(vid):
    v = Video.get_by_id(vid)
    if not v:
        abort(404, {'errro': 'Video not found'})
    return v.serialize()


@get('/playlists')
def get_all_playlists():
    return {'data': [playlist.serialize() for playlist in Playlist.get_all()]}


@post('/playlists')
def create_playlist():
    data = request.json
    if 'name' not in data:
        abort(400)
    p = Playlist(name=data['name'])
    p.save()
    response.status = 201
    return p.serialize()


@get('/playlists/<id:re:[a-f0-9-]+>')
def get_playlist(id):
    id = uuid.UUID(id)
    p = Playlist.get_by_id(str(id))
    if not p:
        abort(404)
    return p.serialize()


# Patch is not available in bottle 0.12
@put('/playlists/<id:re:[a-f0-9-]+>')
def update_playlist(id):
    data = request.json
    if 'name' not in data:
        abort(400)
    id = uuid.UUID(id)
    p = Playlist.get_by_id(str(id))
    if not p:
        abort(404)
    p.name = data['name']
    p.save()
    return p.serialize()


@delete('/playlists/<id:re:[a-f0-9-]+>')
def delete_playlist(id):
    id = uuid.UUID(id)
    p = Playlist.get_by_id(str(id))
    if not p:
        abort(404)
    p.delete()
    response.status = 204
    return None


@get('/playlists/<id:re:[a-f0-9-]+>/videos')
def list_playlist_videos(id):
    id = uuid.UUID(id)
    p = Playlist.get_by_id(str(id))
    if not p:
        abort(404)
    return p.serialize_items()


@put('/playlists/<id:re:[a-f0-9-]+>/videos/<vid:int>')
def add_video_to_playlist(id, vid):
    id = uuid.UUID(id)
    p = Playlist.get_by_id(str(id))
    if not p:
        abort(404, {'errro': 'Playlist not found'})
    v = Video.get_by_id(vid)
    if not v:
        abort(404, {'errro': 'Video not found'})
    p.add_video(v)
    return p.serialize()


@delete('/playlists/<id:re:[a-f0-9-]+>/videos/<vid:int>')
def del_video_from_playlist(id, vid):
    id = uuid.UUID(id)
    p = Playlist.get_by_id(str(id))
    if not p:
        abort(404, {'errro': 'Playlist not found'})
    p.delete_video(vid)
    return p.serialize()
