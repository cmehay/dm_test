import uuid

from .mysql import Mysql


class Playlist(object):

    name = None
    id = None
    in_db = False

    def __init__(self, name, id=None):
        self.id = id or uuid.uuid4()
        self.name = name

    def get_length(self):
        with Mysql() as msql:
            with msql.cursor() as cur:
                cur.execute(
                    '''
                    SELECT COUNT(*) FROM `video`
                    JOIN `playlist_content`
                    ON video.id = playlist_content.video_id
                    WHERE playlist_content.playlist_id = %s;
                    ''', (str(self.id),)
                )
                return cur.fetchone()['COUNT(*)']

    @classmethod
    def get_by_id(cls, id):
        with Mysql() as msql:
            with msql.cursor() as cur:
                cur.execute(
                    '''
                    SELECT `id`, `name`
                    FROM `playlist` WHERE `id`=%s;
                    ''', (id,)
                )
                item = cur.fetchone()
        if not item:
            return None
        p = cls(**item)
        p.in_db = True
        return p

    @classmethod
    def get_all(cls):
        with Mysql() as msql:
            with msql.cursor() as cur:
                cur.execute(
                    '''
                    SELECT `id`, `name`
                    FROM `playlist`;
                    ''',
                )
                items = cur.fetchall()
        return (cls(**item) for item in items)

    def get_videos(self):
        with Mysql() as msql:
            with msql.cursor() as cur:
                cur.execute(
                    '''
                    SELECT `id`, `title`, `thumbnail` FROM `video`
                    JOIN `playlist_content`
                    ON video.id = playlist_content.video_id
                    WHERE playlist_content.playlist_id = %s
                    ORDER BY playlist_content.idx;
                    ''', (str(self.id),)
                )
                items = cur.fetchall()
        return (Video(**item) for item in items)

    def get_video(self, video_id):
        with Mysql() as msql:
            with msql.cursor() as cur:
                cur.execute(
                    '''
                    SELECT `id`, `title`, `thumbnail` FROM `video`
                    JOIN `playlist_content`
                    ON video.id = playlist_content.video_id
                    WHERE playlist_content.playlist_id = %s AND
                    video.id = %s;
                    ''', (str(self.id), video_id,)
                )
                item = cur.fetchone()
        if not item:
            return None
        return Video(**item)

    def get_video_index(self, video_id):
        with Mysql() as msql:
            with msql.cursor() as cur:
                cur.execute(
                    '''
                    SELECT `idx` FROM `playlist_content`
                    WHERE `video_id`=%s AND `playlist_id`=%s;
                    ''', (video_id, str(self.id),)
                )
                item = cur.fetchone()
        return item['idx']

    def add_video(self, video):
        'Idempotent video insert'
        if self.get_video(video.id):
            return
        len = self.get_length()
        with Mysql() as msql:
            with msql.cursor() as cur:
                cur.execute(
                    '''
                    INSERT INTO playlist_content (playlist_id, video_id, idx)
                    VALUES (%s, %s, %s);
                    ''', (str(self.id), str(video.id), len)
                )
            msql.commit()

    def delete_video(self, video_id):
        'Idempotent video delete'
        if not self.get_video(video_id):
            return
        idx = self.get_video_index(video_id)
        with Mysql() as msql:
            msql.begin()
            with msql.cursor() as cur:
                cur.execute(
                    '''
                    DELETE FROM playlist_content
                    WHERE playlist_id=%s AND video_id=%s;
                    ''', (str(self.id), str(video_id))
                )
                cur.execute(
                    '''
                    UPDATE `playlist_content`
                    SET `idx` = `idx` - 1 WHERE `idx` > %s;
                    ''', (idx,)
                )
            msql.commit()

    def delete(self):
        'Itempotent playlist delete'
        if not self.in_db:
            return
        with Mysql() as msql:
            msql.begin()
            try:
                with msql.cursor() as cur:
                    cur.execute(
                        '''
                        DELETE FROM playlist
                        WHERE id=%s;
                        ''', (str(self.id),)
                    )
                msql.commit()
            except BaseException:
                msql.rollback()
                raise
        self.in_db = False

    def serialize(self):
        return {
            'id': str(self.id),
            'name': str(self.name),
            'length': self.get_length(),
        }

    def serialize_items(self):
        return {
            'data': [item.serialize() for item in self.get_videos()]
        }

    def save(self, all=False):
        'Save playlist into database'
        if self.in_db:
            sql = '''
            UPDATE `playlist` SET `name` = %s WHERE id = %s;
            '''
            params = (self.name, str(self.id))
        else:
            sql = '''
            INSERT INTO `playlist` (name, id) VALUES
            (%s, %s);
            '''
            params = (self.name, str(self.id))

        with Mysql() as msql:
            with msql.cursor() as cur:
                cur.execute(sql, params)
            msql.commit()
        self.in_db = True


class Video(object):

    title = None
    id = None
    thumbnail = None

    def __init__(self, title, thumbnail, id):
        self.id = id
        self.title = title
        self.thumbnail = thumbnail

    @classmethod
    def get_by_id(cls, id):
        'Fetch video into database'
        with Mysql() as msql:
            with msql.cursor() as cur:
                cur.execute(
                    '''
                    SELECT `id`, `title`, thumbnail
                    FROM `video` WHERE `id`=%s;
                    ''', (id,)
                )
                item = cur.fetchone()
        if not item:
            return None
        return cls(**item)

    @classmethod
    def get_all(cls):
        'Fetch all videos into database'
        with Mysql() as msql:
            with msql.cursor() as cur:
                cur.execute(
                    '''
                    SELECT `id`, `title`, thumbnail
                    FROM `video`;
                    '''
                )
                items = cur.fetchall()
        return (cls(**item) for item in items)

    def serialize(self):
        return {
            'id': str(self.id),
            'title': str(self.title),
            'thumbnail': str(self.thumbnail),
        }
