from .playlist import Video, Playlist


def test_video():
    v = Video.get_by_id(1)
    assert v.id == 1
    assert v.title == 'video_1'
    assert v.thumbnail == 'path/to/video_1.png'

    all = Video.get_all()
    assert len(list(all)) == 99

    assert not Video.get_by_id(100)


def test_playlists():
    p = Playlist(name='test')
    p.save()
    assert p.in_db

    id = str(p.id)

    p = Playlist.get_by_id(id)
    assert p.id == id

    p.name = 'test2'
    p.save()

    p = Playlist.get_by_id(id)
    assert p.name == 'test2'

    p.delete()
    assert not p.in_db

    assert not Playlist.get_by_id(id)


def test_playlist_content():
    p = Playlist(name='test')
    p.save()

    v1 = Video.get_by_id(1)
    v2 = Video.get_by_id(2)
    v4 = Video.get_by_id(4)

    p.add_video(v1)

    v = p.get_video(v1.id)

    assert v.id == v1.id
    assert v.title == v1.title
    assert v.thumbnail == v1.thumbnail

    assert not p.get_video(v2.id)

    p.add_video(v2)
    p.add_video(v4)
    assert p.get_video_index(v1.id) == 0
    assert p.get_video_index(v2.id) == 1
    assert p.get_video_index(v4.id) == 2

    vl = [v1.id, v2.id, v4.id]

    pl = [v.id for v in p.get_videos()]

    assert vl == pl

    p.delete_video(v2.id)
    assert not p.get_video(v2.id)
    assert p.get_video_index(v1.id) == 0
    assert p.get_video_index(v4.id) == 1

    p.delete()
