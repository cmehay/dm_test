FROM python:3.7

RUN pip install --upgrade pipenv pip

COPY Pipfile* /usr/src/app/

WORKDIR /usr/src/app/
RUN pipenv install --system --verbose --ignore-pipfile

COPY . /usr/src/app/

RUN python3 setup.py install

ENV API_BIND 0.0.0.0

CMD ["api_run_server"]
